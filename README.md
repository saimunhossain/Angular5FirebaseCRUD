# Angular5FirebaseCRUD
This is a Anglar5 with firebase database employee register CRUD. Here you can create, edit and delete employees in a single page that means without page loading and realtime database. 

## Screenshots
![ScreenShot](/screenshot/screenshot1.PNG)
![ScreenShot](/screenshot/screenshot2.PNG)

## Installation
```
git clone
cd Angular5FirebaseCRUD
npm install
```
Now you have to configure with firebase database. Go to https://console.firebase.google.com & click on Add Project & give project name & create Project. 
Then It will show firebase dashboard. In the left side click on database. Click on Get Started & then Rules Tab. Your code should be exact like this
```
{
  "rules": {
    ".read": true,
    ".write": true
  }
}
```
Now Hit on Publish button & Dismiss warning message. And back to Project Overview. Click Add firebase to your web app, and it will pop up bunch of codes. 
Copy configuration object from there and paste it in environment.ts file inside environments folder under src folder. Create new property firebase and 
paste the config object there.
Then Angularfire2 & Firebase package run this command
```
npm install firebase@4.6.1 angularfire2@5.0.0-rc.3 --save
```
For Toastr package run this one
```
npm install ngx-toastr --save
```
To open the project on browser run this command below
```
ng serve --open
```